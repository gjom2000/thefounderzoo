﻿using UnityEngine;


namespace AmazingAssets.CurvedWorld.Example
{
    public class CameraFollow : MonoBehaviour
    {
        public Transform target;            // The position that that camera will be following.
        public float smoothing = 5f;        // The speed with which the camera will be following.


        private Vector3 posOffset;                     // The initial offset from the target.
        private Quaternion targetRot;
        private Vector3 defaultPos;
        private Quaternion defaultRot;
        private Vector3 curvedPos;
        private Quaternion curvedRot;


        void Start ()
        {
            // Calculate initial offset.
            // offset = transform.position - target.position;

            defaultPos = new Vector3(0.0f, 12.5f, 0.0f);
            defaultRot = Quaternion.Euler(new Vector3(80.0f, 0.0f, 0.0f));
            curvedPos = new Vector3(0.0f, 7.5f, -5.0f);
            curvedRot = Quaternion.Euler(new Vector3(45.0f, 0.0f, 0.0f));

            posOffset = defaultPos;
            targetRot = defaultRot;
        }

        public void CurveOn()
        {
            posOffset = curvedPos;
            targetRot = curvedRot;
        }

        public void CurveOff()
        {
            posOffset = defaultPos;
            targetRot = defaultRot;
        }

        void LateUpdate ()
        {
            // Create a postion the camera is aiming for based on the offset from the target.
            Vector3 targetCamPos = target.position + posOffset;

            // Smoothly interpolate between camera's current position and it's target position.
            transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, smoothing * Time.deltaTime);
        }
    }
}
