using AmazingAssets.CurvedWorld;
using AmazingAssets.CurvedWorld.Example;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public CurvedWorldController cwctrl;
    public CameraFollow cameraFollow;
    private bool isCurvedMap = false;
    // Start is called before the first frame update
    void Start()
    {
        cwctrl.CurveOff();
        cameraFollow.CurveOff();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            isCurvedMap = !isCurvedMap;
            if (isCurvedMap)
            {
                cwctrl.CurveOn();
                cameraFollow.CurveOn();
            }
            else
            {
                cwctrl.CurveOff();
                cameraFollow.CurveOff();
            }
        }
    }
}
