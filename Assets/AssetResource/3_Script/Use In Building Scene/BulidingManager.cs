﻿using AmazingAssets.CurvedWorld;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BulidingManager : MonoBehaviour
{
    public CurvedWorldController cwctrl;
    public CameraFollow_Building cameraFollow;
    public PlayerController_Building playerController;
    public Building build;
    public GameObject[] canvas;

    [HideInInspector]
    public static bool isCurved = true;

    // 초기화
    private void Awake()
    {
        isCurved = true;
        this.SetCanvas(canvas[0], isCurved);
        this.SetCanvas(canvas[1], !isCurved);
        cwctrl.CurveOn();
        cameraFollow.Init();
        cameraFollow.CurveOn();
    }
    private void Update()
    {
        // 플레이어 이동
        playerController.GetInput();
        playerController.CalcMoveVec();
        playerController.Move();
        playerController.GetRotation();
        // 오브젝트 놓기
        build.PlaceObject();
    }
    private void FixedUpdate()
    {
        // 오브젝트 놓을 좌표 계산
        build.SearchPlaceToBuild();
    }
    private void LateUpdate()
    {
        // 카메라 추적 활성화
        cameraFollow.Activate();
    }
    private void SetCanvas(GameObject _canvas, bool b)
    {
        // 모드에 따라 캔버스 변경
        _canvas.SetActive(b);
    }
    public void EnterBuildMode() 
    {
        // isCurved 값 변경
        isCurved = !isCurved;
        // 빌드 모드가 아닐 때,
        cwctrl.CurveOff();
        cameraFollow.CurveOff();

        this.SetCanvas(canvas[0], isCurved);
        this.SetCanvas(canvas[1], !isCurved);
    }
    public void ExitBuildMode()
    {
        // isCurved 값 변경
        isCurved = !isCurved;
        // 빌드 모드일 때,
        cwctrl.CurveOn();
        cameraFollow.CurveOn();

        this.SetCanvas(canvas[0], isCurved);
        this.SetCanvas(canvas[1], !isCurved);
    }
    public void SelectObject(int index)
    {
        // 버튼 눌렀을 시 index 값을 토대로 오브젝트 Instantiate
        build.SelectObject(index);
    }
}
