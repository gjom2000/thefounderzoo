﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_Building : MonoBehaviour
{
    private float hAxis;        // -1 ~ 1 수평축 키 입력받을 변수
    private float vAxis;        // -1 ~ 1 수직축 키 입력받을 변수
    private float hAxist;       // 수평축 포지션
    private float vAxist;       // 수직축 포지션
    private float axiss = 10;    // 축 이동속도
    private float speed = 7.5f; // 플레이어 이동속도
    private Vector3 moveVec;    // 플레이어 실제 이동 방향

    private float nowAngle;
    private float targetAngle;
    private float angleOffset;
    private Quaternion rot;


    internal void GetInput() // 방향키 입력
    {
        // 노말 모드가 아니라면(ex : 건설 모드이라면,) 플레이어의 입력을 받지 않음. 
        if (!BulidingManager.isCurved)
            return;

        hAxis = Input.GetAxisRaw("Horizontal");
        vAxis = Input.GetAxisRaw("Vertical");
    }

    internal void CalcMoveVec() // 이동 방향 계산
    {
        if (hAxis == 0 && Mathf.Abs(hAxist) < 0.1f) // 수평축 정지
            hAxist = 0;
        else if ((hAxis == 1 && hAxist < 1) || (hAxis == 0 && hAxist < 0)) // 수평축 증가
            hAxist += axiss * Time.deltaTime;
        else if ((hAxis == -1 && hAxist > -1) || (hAxis == 0 && hAxist > 0)) // 수평축 감소
            hAxist -= axiss * Time.deltaTime;

        if (vAxis == 0 && Mathf.Abs(vAxist) < 0.1f) // 수직축 정지
            vAxist = 0;
        else if ((vAxis == 1 && vAxist < 1) || (vAxis == 0 && vAxist < 0)) // 수직축 증가
            vAxist += axiss * Time.deltaTime;
        else if ((vAxis == -1 && vAxist > -1) || (vAxis == 0 && vAxist > 0)) // 수직축 감소
            vAxist -= axiss * Time.deltaTime;
    }

    internal void Move()
    {
        moveVec = new Vector3(hAxist, 0, vAxist); // 3차원에서의 이동방향
        if (Mathf.Abs(hAxist) + Mathf.Abs(vAxist) > 1)
            moveVec = moveVec.normalized; // 해당 벡터의 크기가 1을 넘어갈 경우 1로 초기화 시켜줌

        transform.position += moveVec * speed * Time.deltaTime; // 플레이어 이동
    }

    internal void GetRotation()
    {
        if (moveVec.magnitude < 0.5f)
            return;

        // Angle
        targetAngle = Mathf.Atan2(hAxist, vAxist) * Mathf.Rad2Deg;
        angleOffset = (targetAngle - nowAngle);
        if (angleOffset > 180)
        {
            nowAngle += 360;
            angleOffset -= 360;
        }
        else if (angleOffset < -180)
        {
            nowAngle -= 360;
            angleOffset += 360;
        }
        nowAngle += angleOffset * Time.deltaTime * 10;

        // Angle to Vector
        rot = Quaternion.Euler(new Vector3(0, nowAngle, 0));
        transform.rotation = rot;
    }
}
