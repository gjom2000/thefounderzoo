﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow_Building : MonoBehaviour // AmazingAssets CameraFollow 개인수정
{
    public Transform playerTransfrom;   // 기본적으로 카메라가 쫒아가게 될 플레이어 Transform
    public Transform buildTransfrom;    // 건설 모드 진입 시 기준이 될 Transfrom
    public float defaultYvalue = 12.5f;
    public float smoothing = 5f;

    private Transform target;   // 기본 : playerTransform     건설 : buildTransform
    private Vector3 posOffset;
    private Quaternion targetRot;
    private Vector3 defaultPos;
    private Quaternion defaultRot;
    private Vector3 curvedPos;
    private Quaternion curvedRot;

    // 초기화 
    internal void Init()
    {
        // Calculate initial offset.
        // offset = transform.position - target.position;

        // 시작 시 target은 playerTransform 으로 초기화
        target = playerTransfrom;

        defaultPos = new Vector3(0.0f, defaultYvalue, 0.0f);
        defaultRot = Quaternion.Euler(new Vector3(80.0f, 0.0f, 0.0f));
        curvedPos = new Vector3(0.0f, 7.5f, -5.0f);
        curvedRot = Quaternion.Euler(new Vector3(45.0f, 0.0f, 0.0f));

        posOffset = curvedPos;
        targetRot = curvedRot;
    }

    internal void CurveOn()
    {
        // 일반 모드 진입 시 target은 playerTransform 으로 변경
        target = playerTransfrom;

        posOffset = curvedPos;
        targetRot = curvedRot;
    }

    internal void CurveOff()
    {
        // 건설 모드 진입 시 target은 buildTransform 으로 변경
        target = buildTransfrom;

        posOffset = defaultPos;
        targetRot = defaultRot;
    }

    internal void Activate()
    {
        // Create a postion the camera is aiming for based on the offset from the target.
        Vector3 targetCamPos = target.position + posOffset;

        // Smoothly interpolate between camera's current position and it's target position.
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, smoothing * Time.deltaTime);
    }
}
