using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Building : MonoBehaviour
{
    public GameObject[] objects;
    private GameObject pendingObject;

    private Vector3 pos;
    private Vector3 localPos;
    private RaycastHit hit;

    [SerializeField] private LayerMask layerMask;

    // 떨어질 grid 단위 (커질 수록 오브젝트들간 배치 길이가 길어짐)
    public float gridSize;

    internal void PlaceObject()
    {
        if(pendingObject != null)
        {
            pendingObject.transform.position = localPos + new Vector3(
                RoundToNearestGrid(pos.x),
                RoundToNearestGrid(pos.y),
                RoundToNearestGrid(pos.z)
                );
            if (Input.GetMouseButtonDown(0))
            {
                pendingObject = null;    // 오브젝트 조작 종료
            }
        }
    }
    // Inspector 창에서 골라진 layerMask에서 마우스 위치 좌표로 환산
    internal void SearchPlaceToBuild()
    {
        if(pendingObject != null)   // 오브젝트가 선택되어 있을 때에만 좌표 계산
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 1000, layerMask))
            {
                pos = new Vector3(hit.point.x, 0, hit.point.z);
            }
        }
    }
    // 버튼 클릭 시 오브젝트 Instantiate 할 함수 (버튼 연동은 Manager.cs 에서)
    internal void SelectObject(int index)
    {
        localPos = objects[index].transform.position;                           // prefab의 localPosition을 따로 저장.
        pendingObject = Instantiate(objects[index], pos, transform.rotation);
    }
    // Grid 계산 함수
    internal float RoundToNearestGrid(float pos)
    {
        float xDiff = pos % gridSize;
        pos -= xDiff;
        if(xDiff > (gridSize / 2))
        {
            pos += gridSize;
        }
        return pos;
    }
}
